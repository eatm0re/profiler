package net.edubovit.load.profiler.domain;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class Metric {

    private LocalDateTime timestamp;
    private Operation operation;
    private String result;
    private int latency;

    public boolean success() {
        return "OK".equals(result);
    }

    public static enum Operation {
        READ, WRITE
    }

}
