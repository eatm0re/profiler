package net.edubovit.load.profiler.domain;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class MetricsSummary {

    private LocalDateTime lastUpdate;
    private int avg;
    private int max;
    private int top5;
    private int top1;
    private int top01;
    private int min;
    private int success;
    private int fail;
    private int total;

    public void addToAvg(int increment) {
        avg += increment;
    }

}
