package net.edubovit.load.profiler.repository;

import net.edubovit.load.profiler.domain.Metric;
import net.edubovit.load.profiler.domain.MetricsSummary;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import static java.util.Comparator.comparing;

@Repository
@Slf4j
public class MetricsRepository {

    private final Queue<Metric> metricsQueueAll = new ConcurrentLinkedQueue<>();
    private final Queue<Metric> metricsQueueRead = new ConcurrentLinkedQueue<>();
    private final Queue<Metric> metricsQueueWrite = new ConcurrentLinkedQueue<>();

    private final List<MetricsSummary> summariesAll = new ArrayList<>(10_000);
    private final List<MetricsSummary> summariesRead = new ArrayList<>(10_000);
    private final List<MetricsSummary> summariesWrite = new ArrayList<>(10_000);

    public void pushMetric(Metric metric) {
        metricsQueueAll.add(metric);
        switch (metric.getOperation()) {
            case READ -> metricsQueueRead.add(metric);
            case WRITE -> metricsQueueWrite.add(metric);
        }
    }

    public MetricsSummary getLastSummary() {
        return summariesAll.get(summariesAll.size() - 1);
    }

    public List<MetricsSummary> getAllSummaries() {
        return summariesAll;
    }

    public MetricsSummary getLastSummaryRead() {
        return summariesRead.get(summariesRead.size() - 1);
    }

    public List<MetricsSummary> getAllSummariesRead() {
        return summariesRead;
    }

    public MetricsSummary getLastSummaryWrite() {
        return summariesWrite.get(summariesWrite.size() - 1);
    }

    public List<MetricsSummary> getAllSummariesWrite() {
        return summariesWrite;
    }

    @Scheduled(fixedRateString = "${profiler.metrics-update-interval}", initialDelayString = "${profiler.metrics-update-interval}")
    public void aggregateMetrics() {
        log.info("Updating metrics...");
        summariesRead.add(aggregateMetrics(metricsQueueRead));
        summariesWrite.add(aggregateMetrics(metricsQueueWrite));
        summariesAll.add(aggregateMetrics(metricsQueueAll));
        log.info("Metrics have been updated");
    }

    private MetricsSummary aggregateMetrics(Collection<Metric> metrics) {
        Metric[] metricsArray = metrics.toArray(Metric[]::new);
        metrics.clear();
        var summary = new MetricsSummary();
        summary.setLastUpdate(LocalDateTime.now());
        summary.setTotal(metricsArray.length);
        summary.setMin(Integer.MAX_VALUE);
        for (var metric : metricsArray) {
            if (metric.success()) {
                summary.addToAvg(metric.getLatency());
                if (metric.getLatency() > summary.getMax()) {
                    summary.setMax(metric.getLatency());
                }
                if (metric.getLatency() < summary.getMin()) {
                    summary.setMin(metric.getLatency());
                }
            }
        }
        metricsArray = Arrays.stream(metricsArray)
                .filter(Metric::success)
                .sorted(comparing(Metric::getLatency))
                .toArray(Metric[]::new);
        summary.setSuccess(metricsArray.length);
        summary.setFail(summary.getTotal() - metricsArray.length);
        summary.setAvg(summary.getSuccess() > 0 ? summary.getAvg() / summary.getSuccess() : -1);
        summary.setTop5(metricsArray.length > 0 ? metricsArray[(int) (metricsArray.length * .95)].getLatency() : -1);
        summary.setTop1(metricsArray.length > 0 ? metricsArray[(int) (metricsArray.length * .99)].getLatency() : -1);
        summary.setTop01(metricsArray.length > 0 ? metricsArray[(int) (metricsArray.length * .999)].getLatency() : -1);
        return summary;
    }

}
