package net.edubovit.load.profiler.scheduler;

import net.edubovit.load.profiler.domain.Metric;
import net.edubovit.load.profiler.repository.MetricsRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static net.edubovit.load.profiler.domain.Metric.Operation.READ;
import static net.edubovit.load.profiler.domain.Metric.Operation.WRITE;

@Component
@Slf4j
@RequiredArgsConstructor
public class ProfilerScheduler {

    @Value("${determinant.url}")
    private String determinantUrl;

    @Value("${profiler.generate-at-start}")
    private int generateAtStart;

    @Value("${profiler.write-rate}")
    private float writeRate;

    private final Random random = new Random();
    private final List<Long> generatedSeeds = new ArrayList<>(10_000_000);
    private final HttpClient client = HttpClient.newHttpClient();

    private final MetricsRepository metricsRepository;

    @Async
    @Scheduled(fixedRateString = "${profiler.rate}")
    public void profile() throws InterruptedException {
        var request = buildRequest();
        long time = System.nanoTime();
        try {
            var response = client.send(request, HttpResponse.BodyHandlers.discarding());
            time = (System.nanoTime() - time) / 1_000_000;
            if (response.statusCode() == 200) {
                log.info("OK - {}ms", time);
                metricsRepository.pushMetric(Metric.builder()
                        .timestamp(LocalDateTime.now())
                        .operation("GET".equals(request.method()) ? READ : WRITE)
                        .result("OK")
                        .latency((int) time)
                        .build());
            } else {
                log.warn("{} - {}ms", response.statusCode(), time);
                metricsRepository.pushMetric(Metric.builder()
                        .timestamp(LocalDateTime.now())
                        .operation("GET".equals(request.method()) ? READ : WRITE)
                        .result(Integer.toString(response.statusCode()))
                        .latency((int) time)
                        .build());
            }
        } catch (IOException e) {
            time = (System.nanoTime() - time) / 1_000_000;
            log.warn("{} - {}ms", e.getMessage(), time);
            metricsRepository.pushMetric(Metric.builder()
                    .timestamp(LocalDateTime.now())
                    .operation("GET".equals(request.method()) ? READ : WRITE)
                    .result(e.getMessage())
                    .latency((int) time)
                    .build());
        }
    }

    private HttpRequest buildRequest() {
        if (generatedSeeds.size() < generateAtStart || random.nextFloat() < writeRate) {
            return buildWriteRequest();
        } else {
            return buildReadRequest();
        }
    }

    private HttpRequest buildWriteRequest() {
        long seed = random.nextLong();
        synchronized (generatedSeeds) {
            generatedSeeds.add(seed);
        }
        return HttpRequest.newBuilder(URI.create(determinantUrl + "/" + seed))
                .POST(HttpRequest.BodyPublishers.noBody())
                .build();
    }

    private HttpRequest buildReadRequest() {
        long seed = generatedSeeds.get(random.nextInt(generatedSeeds.size()));
        return HttpRequest.newBuilder(URI.create(determinantUrl + "/" + seed + "/matrix")).build();
    }

}
