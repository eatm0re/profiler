package net.edubovit.load.profiler.web;

import net.edubovit.load.profiler.domain.MetricsSummary;
import net.edubovit.load.profiler.repository.MetricsRepository;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class MetricsController {

    private final MetricsRepository metricsRepository;

    @GetMapping("metrics")
    public MetricsSummary getMetrics() {
        return metricsRepository.getLastSummary();
    }

    @GetMapping("metrics/all")
    public List<MetricsSummary> getAllMetrics() {
        return metricsRepository.getAllSummaries();
    }

    @GetMapping("metrics/read")
    public MetricsSummary getMetricsRead() {
        return metricsRepository.getLastSummaryRead();
    }

    @GetMapping("metrics/read/all")
    public List<MetricsSummary> getAllMetricsRead() {
        return metricsRepository.getAllSummariesRead();
    }

    @GetMapping("metrics/write")
    public MetricsSummary getMetricsWrite() {
        return metricsRepository.getLastSummaryWrite();
    }

    @GetMapping("metrics/write/all")
    public List<MetricsSummary> getAllMetricsWrite() {
        return metricsRepository.getAllSummariesWrite();
    }

}
